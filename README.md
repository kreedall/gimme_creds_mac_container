* This project expects you have a .okta_aws_login_config file already present on your mac in ~/ or /Users/<yourusername>/

* If you dont have a ~/.okta_aws_login_config file already, copy/paste the contents below to `/Users/username/.okta_aws_login_config`
* Edit the .okta_aws_login_config file and add your username to the okta_username = field

```
[DEFAULT]
okta_org_url = https://compassion.okta.com
okta_auth_server =
client_id =
gimme_creds_server = appurl
aws_appname =
aws_rolename =
write_aws_creds = True
cred_profile = Default
okta_username =
app_url = https://compassion.okta.com/home/amazon_aws/0oa46duhy0khhcUOA2p7/272
resolve_aws_alias = True
include_path = False
preferred_mfa_type = token:software:totp
remember_device = False
aws_default_duration = 36000
device_token =
```

* To build the container:
  * clone this repo locally 
  * `cd path/to/cloned/repo`
  * `docker build . -t gimme-aws-creds`

* To run the container + use gimme-aws-creds in the container, run this command
```
docker run \
    -it \
    --rm \
    -v ~/.aws:/root/.aws \
    -v ~/.okta_aws_login_config:/root/.okta_aws_login_config \
    gimme-aws-creds
```

* To run the container without building it yourself:
```
docker run \
    -it \
    --rm \
    -v ~/.aws:/root/.aws \
    -v ~/.okta_aws_login_config:/root/.okta_aws_login_config \
    registry.gitlab.com/kreedall/gimme_creds_mac_container
```
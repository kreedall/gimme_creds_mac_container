# docker build . -t gimme-aws-creds

FROM python:3.9.15-alpine3.16

RUN pip install --upgrade \
      pip 
RUN pip install \
      awscli \
      gimme-aws-creds==2.4.4

ENTRYPOINT ["gimme-aws-creds"]